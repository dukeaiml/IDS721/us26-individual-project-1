# Static Website using Zola hosted on Vercel
# Demo Video

![](Screen_Recording_2024-04-22_at_7.42.16_PM.mov)

# Access the website

1. Gitlab - https://us26-webpage-udyansachdev-ec56d3741f35a6fe7a13c49f013f37240b0f6.gitlab.io
2. netlify - https://660739c2b18ed86528608b9c--udyan1.netlify.app

Welcome to the documentation for IDS721 Individual Project 01, where we'll walk you through the process of creating a static website using Zola and hosting it on Vercel. This comprehensive guide will cover everything from setup to deployment, ensuring you have a smooth and hassle-free experience.

## Overview

In this project, we'll be using Zola, a fast and flexible static site generator, to create a personal profile website. The chosen theme for our website is [Particle](https://www.getzola.org/themes/particle/), which offers a sleek and professional design. Our website will showcase various details such as basic information, education, work experience, skills, and projects. Once created, we'll host our static website on Vercel, a cloud platform renowned for its simplicity and reliability in hosting static sites and Serverless Functions.

## Vercel Hosting

Vercel provides an excellent platform for hosting static websites, offering features like continuous deployment (CD) and seamless integration with GitLab. With Vercel, every push to the main branch triggers a new deployment of the site, ensuring that your website is always up to date. Additionally, Vercel's integration with GitLab enables a smooth workflow for continuous integration (CI) and deployment (CD), streamlining the development process.

## Webpage visuals

![alt text](screenshots/1.png)

![alt text](screenshots/2.png)

![alt text](screenshots/3.png)

![alt text](screenshots/4.png)


## Setup Instructions

### Cloning the Repository

First, let's clone the project repository to get started:

```bash
git clone https://gitlab.com/dukeaiml/IDS721/us26-individual-project-1.git
cd us26-individual-project-1
```

### Installing Zola

Before we can build our website, we need to install Zola. Follow the official [Zola installation guide](https://www.getzola.org/documentation/getting-started/installation/) to install Zola on your system.

### Running the Development Server

Once Zola is installed, we can start the development server to preview and edit our site locally:

```bash
zola serve
```

Visit http://127.0.0.1:1111 in your web browser to view your website locally and make any necessary edits.

### Customization

Now that we have our development server up and running, let's customize our website:

- Update the `config.toml` file with your personal information.
- Add or modify content in the `content` directory to reflect your profile details.
- Customize the theme and styles as needed. Explore the Hephaestus theme documentation for customization options and theme-specific features.

### Connecting to Netfliy
To host our website on Netlify, follow these steps:

1. Create an account on [Netlify](https://www.netlify.com/).
2. Install the Netlify CLI by running the following command:

```bash
npm install -g netlify-cli
```

3. Log in to your Netlify account using the CLI:

```bash
netlify login
```

4. Deploy the site to Netlify:

```bash
netlify deploy
```

These steps will guide you through the process of deploying your website to Netlify.

Follow the prompts to complete the deployment process. Once deployed, your website will be live on Netlify, and any future changes pushed to the GitLab repository will trigger automatic deployments.

## Conclusion

Congratulations! You've successfully created a static website using Zola and hosted it on Netlify. By following this guide, you've learned how to leverage powerful tools and platforms to showcase your personal profile online. Feel free to explore further customization options and expand your website as needed. Happy coding!